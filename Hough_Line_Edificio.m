clear all;
close all;
RGB = imread('fachada-line-circle.jpg');
%RGB = imgaussfilt(RGB);
RGB=imresize(RGB,0.5);



I  = rgb2gray(RGB); % convert to intensity
BW = edge(I,'canny' ,[0.01 0.15]); % extract edges 

%figure;imshow(BW);
%[H,T,R] = hough(BW,'RhoResolution',1,'Theta',-90:0.1:89.5);%ok mejor
%[H,T,R] = hough(BW,'RhoResolution',2,'Theta',-90:0.5:89.9); %valido tab
[H,T,R] = hough(BW,'RhoResolution',2,'Theta',-90:89);% esta sirve, tambien buena

% figure;
% subplot(2,1,1);
% imshow(RGB);
% title('ESPOL.jpg');
%   
% subplot(2,1,2); 
% imshow(imadjust(mat2gray(H)),'XData',T,'YData',R,...
%               'InitialMagnification','fit');
% title('Limited theta range Hough transform of ESPOL.jpg');
% xlabel('\theta'), ylabel('\rho');
% axis on, axis normal;
% colormap(hot)

P  = houghpeaks(H,100,'threshold',ceil(0.3*max(H(:)))); %picos en la imagen 
% el segundo parametro es para los picos
%imshow(H,[],'XData',T,'YData',R,'InitialMagnification','fit');

% xlabel('\theta'), ylabel('\rho');
% axis on, axis normal, hold on;
% x = T(P(:,2)); y = R(P(:,1));
% plot(x,y,'s','color','white');



% Find lines and plot them
lines = houghlines(BW,T,R,P,'FillGap',5,'MinLength',7);
figure, imshow(RGB), hold on
max_len = 0;
min_len = 30;
for k = 1:length(lines)
   xy = [lines(k).point1; lines(k).point2];
 
   % Determine the endpoints of the longest line segment
   len = norm(lines(k).point1 - lines(k).point2);
   if ( len > max_len)
      max_len = len;
      xy_long = xy;
   end
   
   if ( len > min_len)
   % Plot beginnings and ends of lines
   plot(xy(1,1),xy(1,2),'x','LineWidth',2,'Color','yellow');
   plot(xy(2,1),xy(2,2),'x','LineWidth',2,'Color','red');
    plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');
    

 
   end   
end

% highlight the longest line segment
plot(xy_long(:,1),xy_long(:,2),'LineWidth',2,'Color','blue');

%             for k=1:100
%                 a = cos( T(P(k,2)))
%                 b = sin( T(P(k,2)))
%                 x0 = a* R(P(k,1))
%                 y0 = b* R(P(k,1))
%                 x1 = x0 + 0*(-b)
%                 y1 = y0 + 0*(a)
%                 x2 = x0 - 1000*(-b)
%                 y2 = y0 - 1000*(a)
%                 plot(x1,y1,x2,y2,'LineWidth',5,'Color','Black');
%             end
     
     %y = ( R(P(1,1)) - x* cos( T(P(1,2))) )/ sin( T(P(1,2)));
    
     
for i = 1:size(P,1)
    th = T(P(i,2));
    rh = R(P(i,1));
    m = -(cos(th)/sin(th));
    b = rh/sin(th);
    x = 1:1000;
    plot(x, m*x+b);
    hold on;
end
     
