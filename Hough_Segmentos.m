clear all;
close all;
RGB = imread('line-fachada_1.jpg');
%RGB = imgaussfilt(RGB);



I  = rgb2gray(RGB); % convert to intensity
BW = edge(I,'canny' ,[0.03 0.15]); % extract edges 
%figure;imshow(BW);

[H,T,R] = hough(BW,'RhoResolution',0.5,'Theta',-90:0.5:89.5);
%[H,T,R] = hough(BW,'RhoResolution',1,'Theta',-90:0.2:89);
%[H,T,R] = hough(BW,'RhoResolution',2,'Theta',-90:89);% esta sirve

% figure;
% subplot(2,1,1);
% imshow(RGB);
% title('ESPOL.jpg');
%   
% subplot(2,1,2); 
% imshow(imadjust(mat2gray(H)),'XData',T,'YData',R,...
%               'InitialMagnification','fit');
% title('Limited theta range Hough transform of ESPOL.jpg');
% xlabel('\theta'), ylabel('\rho');
% axis on, axis normal;
% colormap(hot)


P  = houghpeaks(H,200,'threshold',ceil(0.3*max(H(:)))); %picos en la imagen 
% el segundo parametro es para los picos
%imshow(H,[],'XData',T,'YData',R,'InitialMagnification','fit');

% xlabel('\theta'), ylabel('\rho');
% axis on, axis normal, hold on;
% x = T(P(:,2)); y = R(P(:,1));
% plot(x,y,'s','color','white');



% Find lines and plot them
lines = houghlines(BW,T,R,P,'FillGap',5,'MinLength',5);
figure, imshow(RGB), hold on
max_len = 0;
min_len = 0;
for k = 1:length(lines)
   xy = [lines(k).point1; lines(k).point2];
 
   % Determine the endpoints of the longest line segment
   len = norm(lines(k).point1 - lines(k).point2);
   if ( len > max_len)
      max_len = len;
      xy_long = xy;
   end
   
   if ( len > min_len)
   % Plot beginnings and ends of lines
   plot(xy(1,1),xy(1,2),'x','LineWidth',2,'Color','yellow');
   plot(xy(2,1),xy(2,2),'x','LineWidth',2,'Color','red');
    plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');
    

 
   end   
end

% highlight the longest line segment
plot(xy_long(:,1),xy_long(:,2),'LineWidth',2,'Color','blue');

     x= 0:1000;
      %m = -(cos(th)/sin(th));
      m = -cos( T(P(1,2))) / sin( T(P(1,2)))
      b =  R(P(1,1))/sin( T(P(1,2)));
      y =  m*x+b;
      %plot(x, m*x+b);
     
     
     %y = ( R(P(1,1)) - x* cos( T(P(1,2))) )/ sin( T(P(1,2)));
     plot(x,y,'LineWidth',5,'Color','Black');
     
