clear all;
%close all;

%A = imread('circlesBrightDark.png');
A = imread('circles.jpg');
figure,imshow(A)

Rmin = 16;
Rmax = 35;
[centersBright, radiiBright] = imfindcircles(A,[Rmin Rmax],'ObjectPolarity','bright','Sensitivity',0.91);
[centersDark, radiiDark] = imfindcircles(A,[Rmin Rmax],'ObjectPolarity','dark','Sensitivity',0.97);
viscircles(centersBright, radiiBright,'EdgeColor','b');
viscircles(centersDark, radiiDark,'LineStyle','--');


