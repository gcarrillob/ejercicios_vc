# -*- coding: utf-8 -*-
"""
Created on Sat Jun 11 00:14:03 2016

@author: gcarrillo
"""
import sys
import math
import numpy as np

sys.path.append('/usr/local/lib/python2.7/site-packages')
import cv2
while(1):
    img = cv2.imread("line_ESPOL.jpg")
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    edges = cv2.Canny(gray, 80, 120)
    
    

    lines = cv2.HoughLines(edges,1,np.pi/90,200,0,0)
    for detailine in lines:
        for rho,theta in detailine:
            a = np.cos(theta)
            b = np.sin(theta)
            x0 = a*rho
            y0 = b*rho
            x1 = int(x0 + 1000*(-b))
            y1 = int(y0 + 1000*(a))
            x2 = int(x0 - 1000*(-b))
            y2 = int(y0 - 1000*(a))

        cv2.line(img,(x1,y1),(x2,y2),(0,0,255),2)    
    
    cv2.imshow('edges',img)    
    
    lines = cv2.HoughLinesP(edges, 1, math.pi/2, 2, None, 30, 1);
    for detailine in lines:
        for x1,y1,x2,y2 in detailine:
            cv2.line(img,(x1,y1),(x2,y2),(0,255,0),2)

    cv2.imshow('frame',img)    
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        continue
#cv2.imwrite("C:/temp/2.png", img)