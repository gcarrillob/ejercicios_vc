%http://www.mathworks.com/help/images/examples/detect-and-measure-circular-objects-in-an-image.html
%http://blogs.mathworks.com/steve/2012/09/04/detecting-circular-objects-in-images/
clear all;
close all;

A = imread('Count-circles.jpg');
%A = imread('circles.jpg');
figure,imshow(A)

Rmin = 30;
Rmax = 75;

[centersBright, radiiBright] = imfindcircles(A,[Rmin Rmax],'ObjectPolarity','bright','Sensitivity',0.94,'EdgeThreshold',0.1);

%[centersDark, radiiDark] = imfindcircles(A,[Rmin Rmax],'ObjectPolarity','dark','Sensitivity',0.98);

viscircles(centersBright, radiiBright,'EdgeColor','b');

%viscircles(centersDark, radiiDark,'LineStyle','--');


