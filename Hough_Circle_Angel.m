clear all;
close all;
A = imread('coins.png');
%A = imread('circles.jpg');
imshow(A);
[centers, radii, metric] = imfindcircles(A,[15 30]);
centersStrong5 = centers(1:5,:);
radiiStrong5 = radii(1:5);
metricStrong5 = metric(1:5);

%h = viscircles(centers,radii);
h = viscircles(centersStrong5, radiiStrong5,'EdgeColor','b');

% A = imread('circlesBrightDark.png');
% imshow(A)
% 
% Rmin = 30;
% Rmax = 65;
% 
% [centersBright, radiiBright] = imfindcircles(A,[Rmin Rmax],'ObjectPolarity','bright');
% 
% [centersDark, radiiDark] = imfindcircles(A,[Rmin Rmax],'ObjectPolarity','dark');
% 
% viscircles(centersBright, radiiBright,'EdgeColor','b');
% 
% viscircles(centersDark, radiiDark,'LineStyle','--');
% 
% 
