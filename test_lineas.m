% read the image into MATLAB and convert it to grayscale
%I = imread('test.jpg');
I = imread('fachada-line-circle.jpg');

% We can see that the image is noisy. We will clean it up with a few
% morphological operations


I  = rgb2gray(I); % convert to intensity
BW = edge(I,'canny' ,[0.01 0.15]); % extract edges 

%figure;imshow(BW);
%[H,T,R] = hough(BW,'RhoResolution',0.5,'Theta',-90:0.5:89.5);
%[H,T,R] = hough(BW,'RhoResolution',2,'Theta',-90:0.1:89.9);
[H,T,R] = hough(BW,'RhoResolution',2,'Theta',-90:89);% esta sirve


peaks  = houghpeaks(H,10);
lines = houghlines(Ibw,theta,rho,peaks);
figure, imshow(cleanI)
% Highlight (by changing color) the lines found by MATLAB
hold on
for k = 1:numel(lines)
    x1 = lines(k).point1(1);
    y1 = lines(k).point1(2);
    x2 = lines(k).point2(1);
    y2 = lines(k).point2(2);
    plot([x1 x2],[y1 y2],'Color','b','LineWidth', 2)
end
hold off
% Identify the angles of the lines. 
% The following command shows the angle of the 2nd line found by the Hough
% Transform
lines(1).theta
lines(2).theta